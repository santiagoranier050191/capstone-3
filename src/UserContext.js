// We will use React's Context API to give the logged in user object to have "global" scope within our application.
import React from 'react';

// Create a context object
// A context object is an object that can be used to store information that can be shared to other components within the app
export const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;


export default UserContext;