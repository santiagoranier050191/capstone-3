import React from 'react';

const Banner = ({ data }) => {
  if (!data) {
    
    return null; 
  }

  const { title, subtitle, image } = data;

  return (
    <div className="banner">
      <img src={image} alt="Banner" />
      <div className="banner-content">
        <h1>{title}</h1>
        <p>{subtitle}</p>
      </div>
    </div>
  );
};

export default Banner;