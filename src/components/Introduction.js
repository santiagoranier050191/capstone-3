import React from 'react';
import './Introduction.css';
export default function Introduction() {
  return (
    <div className="intro-container">
      <h1 className="intro">Welcome to the Philippines</h1>
      <p className="intro-message"> COME & DISCOVER
         beautiful beaches, amazing wildlife, stunning landscapes, ocean adventures, cultural and culinary delights, the warmth and hospitality of the Filipino people. It's more fun in the Philippines...</p>
    </div>
  );
}
