import {useContext, useEffect, useState} from "react";
import { Button, Table, Modal, Form } from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function AdminDashboard(){

	const { user } = useContext(UserContext);

	// Create allCourses state to contain the courses from the response of our fetch data.
	const [allTours, setAllTours] = useState([]);

	// State hooks to store the values of the input fields for our modal.
	const [toursd, setToursId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [slots, setSlots] = useState(0);
	const [email, setEmail] = useState("");
	const [isAdmin, setIsAdmin] = useState("");

    // State to determine whether submit button in the modal is enabled or not
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);
	const [showUser, setShowUser] = useState(false);

	// To control the add course modal pop out
	const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal

	const openUser = () => setShowUser(true); //Will show the modal
	const closeUser = () => setShowUser(false); //Will hide the modal

	// To control the edit course modal pop out
	// We have passed a parameter from the edit button so we can retrieve a specific course and bind it with our input fields.
	const openEdit = (id) => {
		setToursId(id);

		// Getting a specific course to pass on the edit modal
		fetch(`${ process.env.REACT_APP_API_URL }/products/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			// updating the course states for editing
			setName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setSlots(data.stock);
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		// Clear input fields upon closing the modal
	    setName('');
	    setDescription('');
	    setPrice(0);
	    setSlots(0);

		setShowEdit(false);
	};


	const fetchData = () =>{
		
		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts/`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllTours(data.map(tours => {
				return (
					<tr key={tours._id}>
						<td>{tours._id}</td>
						<td>{tours.name}</td>
						<td>{tours.description}</td>
						<td>{tours.price}</td>
						<td>{tours.stock}</td>
						<td>{tours.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								
								(tours.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(tours._id, tours.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(tours._id, tours.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(tours._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}

	const fetchSingleData = (id) =>{
		
		fetch(`${process.env.REACT_APP_API_URL}/tour/${id}/`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setSlots(data.stock);
		});
	}

	
	useEffect(()=>{
		fetchData();
	}, [])


	const archive = (id, toursName) =>{
		console.log(id);
		console.log(toursName);

		// Using the fetch method to set the isActive property of the course document to false
		fetch(`${process.env.REACT_APP_API_URL}/tour/${id}/archive/`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${tourName} is now inactive.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	const unarchive = (id, tourName) =>{
		console.log(id);
		console.log(tourName);

		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}/`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${tourName} is now active.`
				});
				
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	
	const addTour = (e) =>{
			
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/tour/createTour/`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    productName: name,
				    description: description,
				    price: price,
				    stock: slots
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Course succesfully Added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the modal
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setSlots(0);
	}


	const setUserAdmin = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/users/changeAdminFlag`, {
		    	method: "PATCH",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    email: email,
				    isAdmin: isAdmin
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	let strAdmin = "";

		    	if (!isAdmin) {
		    		strAdmin = "not "
		    	}

		    	if(data){
		    		Swal.fire({
		    		    title: "Admin flag udpate",
		    		    icon: "success",
		    		    text: `${email} is now ${strAdmin}an Admin`
		    		});

		    		// Automatically closed the modal
		    		closeUser();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setSlots(0);
	}

	// [SECTION] Edit a specific course
	// Updating a specific course in our database
	// edit a specific course
	const editTours = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/edit/${courseId}`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    productName: name,
				    description: description,
				    price: price,
				    stock: slots
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Course succesfully Updated",
		    		    icon: "success",
		    		    text: `${name} is now updated`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the form
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setSlots(0);
	} 

	// Submit button validation for add/edit course
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if((name != "" && description != "" && price > 0 && slots > 0) || (email != "" && (isAdmin == "true" || isAdmin == "false"))){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price, slots, email, isAdmin]);

//				<Button variant="secondary" className="mx-2" as={Link} to={`/allProducts`}>Show All Users</Button>
	return(
		(user.isAdmin)
		?
		<>
			{/*Header for the admin dashboard and functionality for create course and show enrollments*/}
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				{/*Adding a new course */}
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Product</Button>
				<Button variant="success" className="
				mx-2" onClick={openUser}>Make Admin</Button>
				{/*To view all the user enrollments*/}
				<Button variant="primary" className="
				mx-2" as={Link} to={`/myOrderHistory`}>Show All Orders</Button>
			</div>
			{/*End of admin dashboard header*/}

			{/*For view all the courses in the database.*/}
			<Table striped bordered hover>
		      <thead>
		        <tr>
		          <th>Tour ID</th>
		          <th>TourName</th>
		          <th>Description</th>
		          <th>Price</th>
		          <th>Slots</th>
		          <th>Status</th>
		          <th>Actions</th>
		        </tr>
		      </thead>
		      <tbody>
	        	{allTours}
		      </tbody>
		    </Table>
			{/*End of table for course viewing*/}

	    	{/*Modal for Adding a new course*/}
	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
	    		<Form onSubmit={e => addCourse(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Product Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Name" 
	    		                value = {name}
	    		                onChange={e => setName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Product Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter Product Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Product Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Product Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="slots" className="mb-3">
	    	                <Form.Label>Product Stock</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Number of Stocks" 
	    		                value = {slots}
	    		                onChange={e => setSlots(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    	{/*End of modal for adding course*/}

	    	{/*Modal for chanuserging user admin*/}
	        <Modal show={showUser} fullscreen={true} onHide={closeUser}>
	    		<Form onSubmit={e => setUserAdmin(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Cange user [Admin] flag</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="email" className="mb-3">
	    	                <Form.Label>User Email</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Product Name" 
	    		                value = {email}
	    		                onChange={e => setEmail(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="isAdmin" className="mb-3">
	    	                <Form.Label>Flag</Form.Label>
	    	                <Form.Control
	    	                	type="text"
	    		                placeholder="true/false" 
	    		                value = {isAdmin}
	    		                onChange={e => setIsAdmin(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeUser}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    	{/*End of changing user admi*/}


    	{/*Modal for Editing a course*/}
        <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    		<Form onSubmit={e => editCourse(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Course Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Course Name" 
    		                value = {name}
    		                onChange={e => setName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Course Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter Course Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Course Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Course Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="slots" className="mb-3">
    	                <Form.Label>Course Slots</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Course Slots" 
    		                value = {slots}
    		                onChange={e => setSlots(e.target.value)}
    		                required
    	                />
    	            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>
    	{/*End of modal for editing a course*/}
		</>
		:
		<Navigate to="/tours" />
	)
}