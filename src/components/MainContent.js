import React from 'react';

export default function MainContent() {
  return (
    <div className="main-content">
      <h1>Welcome to the Dashboard</h1>
      <p>This is the main content area of the dashboard.</p>
    </div>
  );
}
