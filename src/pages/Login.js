import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { NavLink, Navigate } from 'react-router-dom';
import { UserContext } from '../UserContext';

export default function Login() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);
  const [isLoggingIn, setIsLoggingIn] = useState(false);

  const retrieveUserDetails = (token) => {
    fetch('http://localhost:4000/user/details', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  const authenticate = (e) => {
    e.preventDefault();
    setIsLoggingIn(true);
    fetch('http://localhost:4000/user/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.accessToken !== undefined) {
          localStorage.setItem('token', data.accessToken);
          retrieveUserDetails(data.accessToken);
          Swal.fire({
            title: 'Login Successful',
            icon: 'success',
            text: 'Welcome to Ganda Pilipinas Travel and Tours!',
          });
        } else {
          Swal.fire({
            title: 'Authentication failed!',
            icon: 'error',
            text: 'Check your login credentials and try again!',
          });
        }
      })
      .finally(() => {
        setIsLoggingIn(true);
      });

    setEmail('');
    setPassword('');
  };

  useEffect(() => {
    setIsActive(email !== '' && password !== '');
  }, [email, password]);

  if (user !== null && user.id !== null) {
    return <Navigate to="/tours" />;
  } else if (user !== null && user.isAdmin) {
    return <Navigate to="/dashboard" />;
  }

  return (
    <>
      <Form onSubmit={authenticate}>
        <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>

        {isActive ? (
          <Button variant="primary" type="submit" id="submitBtn" disabled={isLoggingIn}>
            {isLoggingIn ? 'Logging in...' : 'Submit'}
          </Button>
        ) : (
          <Button variant="danger" type="submit" id="submitBtn" disabled>
            Submit
          </Button>
        )}

        <p className="register-text">
          If not registered yet,{" "}
          <NavLink to="/register" className="register-link">
            click here
          </NavLink>{' '}
          to register.
        </p>
      </Form>
    </>
  );
}
