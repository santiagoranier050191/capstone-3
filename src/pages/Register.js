import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { UserContext } from '../UserContext';

export default function Register() {
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  // State hooks to store the values of the input fields
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      mobileNo.length === 11 &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password1, password2]);

  async function checkEmailAvailability(email) {
    try {
      const response = await fetch('http://localhost:4000/user/checkEmail', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
      });
      const data = await response.json();
      return data;
    } catch (error) {
      console.error('Error:', error);
      throw new Error('Error occurred');
    }
  }

  async function registerUser(e) {
    e.preventDefault();

    try {
      const emailAvailable = await checkEmailAvailability(email);

      if (emailAvailable) {
        const response = await fetch('http://localhost:4000/user/register', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            firstName,
            lastName,
            email,
            mobileNo,
            password: password1,
          }),
        });
        const data = await response.json();

        if (data) {
          localStorage.setItem('user', JSON.stringify(data));

          setFirstName('');
          setLastName('');
          setEmail('');
          setMobileNo('');
          setPassword1('');
          setPassword2('');

          Swal.fire({
            title: 'Registration successful',
            icon: 'success',
            text: 'Welcome Ganda Pilipinas Travel and Tours',
          });

          navigate('/login');
        } else {
          Swal.fire({
            title: 'Something went wrong.',
            icon: 'error',
            text: 'Please try again.',
          });
        }
      } else {
        Swal.fire({
          title: 'Duplicate email found',
          icon: 'error',
          text: 'Kindly provide another email to complete the registration.',
        });
      }
    } catch (error) {
      Swal.fire({
        title: 'Something went wrong.',
        icon: 'error',
        text: 'Please try again.',
      });
      setEmail('');
      setPassword1('');
    }
  }

  return (
    <>
      {user && user.id !== null ? (
        <Navigate to="/login" />
      ) : (
        <Form onSubmit={(e) => registerUser(e)}>
          <Form.Group controlId="firstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter first name"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="lastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter last name"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              required
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="mobileNo">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Mobile Number"
              value={mobileNo}
              onChange={(e) => setMobileNo(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              required
              value={password1}
              onChange={(e) => setPassword1(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Verify Password"
              required
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
            />
          </Form.Group>

          {isActive ? (
            <Button variant="primary" type="submit">
              Submit
            </Button>
          ) : (
            <Button variant="danger" type="submit" disabled>
              Submit
            </Button>
          )}
        </Form>
      )}
    </>
  );
}
