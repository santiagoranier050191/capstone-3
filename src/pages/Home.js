import React from 'react';
import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Introduction from '../components/Introduction';
import Footer from '../components/Footer';
import './Home.css';

export default function Home() {
  return (
    <Fragment className="home-button">
      <Introduction />
      <div >
        <Banner />
      </div>
      <Highlights />
      <Footer />
    </Fragment>
  );
}


