import React, { useContext } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import toursData from '../data/toursData';

export default function AddToCart({ tourId }) {
  const { user } = useContext(UserContext);

  const tour = toursData.find((tour) => tour.id === tourId);

 const addToCart = (tourId) => {
  fetch(`${process.env.REACT_APP_API_URL}/user/book`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify({
      tourId: tourId
    })
  })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if (data.success) {
        Swal.fire({
          title: 'Added to Cart!',
          icon: 'success',
          text: 'The tour has been added to your cart.'
        });
      } else {
        Swal.fire({
          title: 'Something went wrong',
          icon: 'error',
          text: 'Please try again.'
        });
      }
    })
    .catch(error => {
      console.log('Error adding to cart:', error);
      Swal.fire({
        title: 'Something went wrong',
        icon: 'error',
        text: 'Please try again.'
      });
    });
};

  return (
  <Card>
    <Card.Body className="text-center">
      <Card.Title>{tour && tour.name}</Card.Title>
      <Card.Subtitle>Description:</Card.Subtitle>
      <Card.Text>{tour && tour.description}</Card.Text>
      <Card.Subtitle>Price:</Card.Subtitle>
      <Card.Text>PhP {tour && tour.price}</Card.Text>
      <Button variant="primary" onClick={() => addToCart(tour && tour.tourId)}>
        Add to Cart
      </Button>
    </Card.Body>
  </Card>
);
}
