import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { UserContext } from '../UserContext';

const Logout = () => {
  const { setUser } = useContext(UserContext);

  const handleLogout = () => {
    setUser(null);
    localStorage.removeItem('token');
    
    return <Navigate to="/" />;
  };

  return handleLogout();
};

export default Logout;
