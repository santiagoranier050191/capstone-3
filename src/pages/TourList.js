import React from 'react';
import TourCard from './TourCard';

export default function TourList({ tours }) {
  return (
    <div>
      <h2>List of Tours</h2>
      {tours.map((tour) => (
        <TourCard key={tour.id} tour={tour} />
      ))}
    </div>
  );
}